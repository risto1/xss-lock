CC = gcc
SRC = xss-lock.c
OBJ = $(SRC:.c=.o)
CFLAGS = `pkg-config --cflags xcb xcb-aux xcb-event xcb-screensaver glib-2.0 gio-unix-2.0`
LDFLAGS = `pkg-config --libs xcb xcb-aux xcb-event xcb-screensaver glib-2.0 gio-unix-2.0`

xss-lock: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ) $(LIBS)

$(OBJ): $(SRC)
	$(CC) $(CFLAGS) -c $(SRC) $(LDFLAGS)

.PHONY: clean
clean:
	rm xss-lock $(OBJ)
